package com.dyplom.nfc;

public interface Listener {

    void onDialogDisplayed();

    void onDialogDismissed();
}
